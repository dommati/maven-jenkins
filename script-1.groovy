def buildApp(){
    echo "building the application ${params.VERSION}"
}

def testApp(){
    echo "testing the application....."
}

def deployApp(){
    echo "deploying the application on prod...."
    echo "deploying the version ${params.VERSION}"
    echo "deploying to ${ENV}"
}

return this
