def buildJar() {
    echo "building the jar file..."
    sh "mvn package"
}

def buildImage() {
    echo "building the docker image.."
    withCredentials([
        usernamePassword(credentialsId:"docker-hub", usernameVariable: "USER", passwordVariable: "PWD")
    ]) {
        sh "docker build -t dsp143/demo-app:jma-1.4 ."
        sh "echo ${PWD} | docker login -u ${USER} --password-stdin"
        sh "docker push dsp143/demo-app:jma-1.4"
    }
}

def deployImage() {
    echo "building the jar file..."
}

return this
